#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "hardware/watchdog.h"
#include "ws2812.pio.h"

#define IS_RGBW true // Flag to indicate if LED strip supports RGBW colours.
#define WS2812_PIN 28 // GPIO pin connected to the WS2812 LED.
#define NUMOFLEVELS 4 // Total num of levels in the game.
#define NUM_CHAR 36 // Total num of alphanumeric characters (0-9, A-Z) in Morse code mapping.
#define NUM_WORDS 10 // Num of words used in advanced levels of the game.
#define MAX_MORSE_SEQUENCE_LENGTH 5 // Max length of a Morse code sequence for one character.
#define MAX_INPUT_LENGTH 40 // Maxlength ofinput buffer to store player's Morse code input.
#define NUM_PIXELS

char playerInput[MAX_INPUT_LENGTH] = ""; // Define and initialize playerInput.
char correctAnswer[MAX_INPUT_LENGTH] = ""; // Define and initialize correctAnswer for storing the correct Morse code or character.
bool strtInput; // Init to true for indicating start of input.

int numOfCharEnt; // For tracking the num of characters entered.
int gameLevel; // Current level of the game the player is on.
int numOfAttempts; // Counts the number of atempts the player has made.
int numOfAnsCorrect; // Initialize the number of correct answers
int numOfAnsIncorrect; // Initialize the number of incorrect answers
bool if_game_completed = false; // Add this line at the global scope to define the variable
int correctAnsStreak = 0; // Tracks th streak of correct answers
int inputlen = 0;  // Length of the input provided by the player.

/**
 * @struct MorseMap
 * @brief Structure to map a character to its Morse code representation.
 */
typedef struct {
    char character;
    const char* morse;
} MorseMap;

/**
 * @struct WordMorseMap
 * @brief Structure to map a word to its Morse code representation.
 */
typedef struct {
    const char* word;
    const char* morse;
} WordMorseMap;


/**
 * @brief Maps alphanumeric characters to their Morse code equivalents.
 * This array provides a mapping between alphanumeric characters ('0'-'9', 'A'-'Z') and their
 * corresponding Morse code sequences as strings.
 */
const MorseMap morseMap[NUM_CHAR] = {
    {'0', "-----"}, {'1', ".----"}, {'2', "..---"}, {'3', "...--"}, {'4', "....-"},
    {'5', "....."}, {'6', "-...."}, {'7', "--..."}, {'8', "---.."}, {'9', "----."},
    {'A', ".-"}, {'B', "-..."}, {'C', "-.-."}, {'D', "-.."}, {'E', "."},
    {'F', "..-."}, {'G', "--."}, {'H', "...."}, {'I', ".."}, {'J', ".---"},
    {'K', "-.-"}, {'L', ".-.."}, {'M', "--"}, {'N', "-."}, {'O', "---"},
    {'P', ".--."}, {'Q', "--.-"}, {'R', ".-."}, {'S', "..."}, {'T', "-"},
    {'U', "..-"}, {'V', "...-"}, {'W', ".--"}, {'X', "-..-"}, {'Y', "-.--"},
    {'Z', "--.."}
};


/**
 * @brief Maps words to their Morse code equivalents.
 * This array is designed for more advanced levels of the Morse code game, providing a mapping
 * between selected words and their corresponding Morse code sequences
 */
const WordMorseMap wordMorseMap[NUM_WORDS] = {
    {"Code", "-.-.---.."}, {"Radio", ".-. .- -.. .. ---"}, {"Light", ".-.. .. --. .... -"}, 
    {"Signal", "... .. --. -. .- .-.."}, {"Morse", "-- --- .-. ... ."}, {"Encode", ". -. -.-. --- -.. ."}, 
    {"Decode", "-.. . -.-. --- -.. ."}, {"Message", "-- . ... ... .- --. ."}, {"Transmit", "- .-. .- -. ... -- .. -"}, 
    {"Receive", ".-. . -.-. . .. ...- ."}
};


/**
 * @brief Entry point for the main assembly code.
 * This function serves as the starting point for assembly code execution.
 */
void main_asm();


/**
 * @brief Initializes a GPIO pin.
 * Configures the specified GPIO pin for use, preparing it for input or output operations.
 */
void asm_gpio_init(uint pin) {
    gpio_init(pin);
}



/**
 * @brief Sets the direction of a GPIO pin.
 * Defines whether a GPIO pin is configured as an input or an output.
 */
void asm_gpio_set_dir(uint pin, bool out) {
    gpio_set_dir(pin, out);
}


/**
 * @brief Reads the value of a GPIO pin.
 * Returns the current logic level (high or low) of the specified GPIO pin.
 */
bool asm_gpio_get(uint pin) {
    return gpio_get(pin);
}



/**
 * @brief Sets the value of a GPIO pin.
 * Calls the SDK's `gpio_put` function to set the specified GPIO pin to the given value.
 */
void asm_gpio_put(uint pin, bool value) {
    gpio_put(pin, value);
}


/**
 * @brief Enables both rising and falling edge interrupts for a GPIO pin.
 * Configures the specified GPIO pin to trigger interrupts on both rising and falling edges.
 */
void asm_gpio_set_irq(uint pin)
{
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL, true);
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE, true);
}


/**
 * @brief Initializes the watchdog timer.
 * Enables the watchdog timer with a timeout of 9 seconds. If the watchdog is not updated
 * within this period via `watchdog_update`, the system will reset
 */
void init_watchdog_timer(){
    watchdog_enable(9000000, 1);
}


/**
 * @brief Updates the watchdog timer.
 * Resets the watchdog timer countdown, preventing the system from resetting
 */
void watchdog_updater(){
    watchdog_update();
}


/**
 * @brief Clears the contents of a string.
 * 
 * This function resets every character in the string to the null terminator '\0',
 * and clears its contents.
 */
void clear_string(char* str) {
    // Assuming MAX_INPUT_LENGTH is the size of your string arrays.
    for (int i = 0; i < MAX_INPUT_LENGTH; i++) {
        str[i] = '\0';
    }
}

/**
 * @brief Sends a pixel color to the WS2812 LED.
 * This inline function writes a 24-bit color value to the WS2812 LED using the PIO's
 * state machine.
 */
static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}


/**
 * @brief Converts individual RGB components into a 24-bit color value.
 */
static inline uint32_t urgb_u32(uint8_t red, uint8_t green, uint8_t blue) {
    return  ((uint32_t) (red) << 8)  |
            ((uint32_t) (green) << 16) |
            (uint32_t) (blue);
}


/**
 * @brief Sets the LED color to blue.
 * Utilizes the `put_pixel` function to change the LED's color to blue by setting its RGB values.
 */
void set_LED_blue() {
    put_pixel(urgb_u32(0x00, 0x00, 0x7F));
}

/**
 * @brief Sets the LED color to green.
 * Changes the LED's color to green using specific RGB values through the `put_pixel` function.
 */
void set_LED_green() {
    put_pixel(urgb_u32(0x00, 0x7F, 0x00));
}

/**
 * @brief Sets the LED color to yellow.
 * Applies yellow color to the LED by passing the appropriate RGB values to the `put_pixel` method.
 */
void set_LED_yellow() {
    put_pixel(urgb_u32(0x7F, 0x7F, 0x00));
}

/**
 * @brief Sets the LED color to orange.
 * Configures the LED to display an orange color by using specific RGB values in the `put_pixel` call.
 */
void set_LED_orange() {
    put_pixel(urgb_u32(0x7F, 0x40, 0x00));
}


/**
 * @brief Sets the LED color to red.
 * Adjusts the LED to a red color by setting the appropriate RGB values with the `put_pixel` function.
 */
void set_LED_red() {
    put_pixel(urgb_u32(0x7F, 0x00, 0x00));
}

int livesLeft = 3; // Assuming the player starts with 3 lives


/**
 * @brief Changes the LED color based on the remaining lives.
 * This function changes the LED's color to reflect the current state of the player's lives
 * in the game. Each number of lives corresponds to a specific color
 */
void change_LED() {
    switch(livesLeft) {
        case 3:
            set_LED_green();
            break;
        case 2:
            set_LED_yellow();
            break;
        case 1:
            set_LED_orange();
            break;
        case 0:
            set_LED_red();
            break;
        default:
            printf("Invalid number of lives.\n");
    }
}



/**
 * @brief Computes the difference between two time points.
 * This function finds the time elapsed between two events, where the times of the
 * events are provided as arguments. 
 */
int get_diff(int current_time, int previous_time) {
    return current_time - previous_time;
}


/**
 * @brief Resets all game-related variables to their initial state.
 * 
 * This function is responsible for reinitializing the game state at the start of the game
 * or when restarting the game after a completion.
 */
void game_var_reset() {
    // Reset game variables
    playerInput[0] = '\0';          // Resetting the player input array
    strtInput = true;            // Player has to start his first input
    if_game_completed = false;        // Is the game complete?
    livesLeft = 3;  // Reset the number of lives
    correctAnsStreak = 0;       // Reset the streak of correct answers
    numOfCharEnt = 0;      // Reset the number of char entered
    numOfAnsCorrect = 0;       // Reset the number of correct answers
    numOfAnsIncorrect = 0;     // Reset the number of incorrect answers
    numOfAttempts = 0;             // Reset the number of tries
    inputlen = 0;
}


/**
 * @brief Initializes and starts the Morse Code Game.
 * 
 * Displays the game's welcome message, instructions, and control schemes to the player. It displays
 * the objective of the game, how to input Morse code using the Pico Board, and the rules regarding
 * the number of lives and levels.
 */
void start_the_game() {
    printf("\n____________________________________________________________________________________________\n");
    printf(" | *        *****       *       *****   *     *      *     *   ***    *****    ****    *****  |\n");
    printf(" | *        *          * *      *    *  * *   *      * * * *  *   *   *    *  *        *      |\n");
    printf(" | *        *****     *****     *****   *  *  *      *  *  *  *   *   *****    ****    *****  |\n");
    printf(" | *        *        *     *    *  *    *   * *      *     *  *   *   *  *         *   *      |\n");
    printf(" | *****    *****   *       *   *    *  *     *      *     *   ***    *    *   ****    *****  |\n");
    printf("\n____________________________________________________________________________________________\n");
    printf("\n Morse Code Game Presented By: Group 33");
    printf("\n Written By: Maya Woolaver, George Brady, Kenneth Johnson, Michael Trearty, and Junpneg Cui\n\n\n");

    printf("Instructions:\n");
    printf("In order to move to the next level you must enter the correct morse code sequence\n");
    printf("The game will provide you a character and you will enter the response using the Pico Board\n");
    printf("You will have 3 lives in order to attempt to get the right sequences for each level\n");

    printf("\nControls:\n");
    printf("A dot is written by pressing GP21 for under .25 seconds\n");
    printf("A dash is written by pressing GP21 for over .25 seconds\n");
    printf("A space is written by leaving the button unpressed for 1 second\n");
    printf("You can submit a final response by leaving the button unpressed for 2 seconds\n");

    printf("\nLevels:\n");
    printf("Level 1 will provide you with a character and its corresponding morse code\n");
    printf("Level 2 will provide you with a character without its corresponding morse code\n");

    game_var_reset();
}

void level_select_msg(){
    printf("\n\n ----------LEVEL SELECT---------- \n\n");
    printf(" Enter .---- for Level 1\n");
    printf(" Enter ..--- for Level 2\n");
    printf(" Enter ...-- for Level 3\n");
    printf(" Enter ....- for Level 4\n");
}





/**
 * @brief Selects a random character or word and its corresponding Morse code for the current level.
 * 
 * Depending on the current game level, this function selects either a random character for levels 1 and 2
 * or a random word and displays it to the player. For level 1 and level 3, it also
 * shows the corresponding Morse code to assist the player. The selected character's or word's Morse code
 * is then stored as the correct answer for the player to input.
 */
void random_char() {
    int randomChar = 0;

    if (gameLevel == 1 || gameLevel == 2) {
        randomChar = rand() % NUM_CHAR; // Use NUM_CHAR for the range
        printf("\nEnter the character: %c", morseMap[randomChar].character);
        if (gameLevel == 1) {
            printf(" or '%s' in Morse Code.\n", morseMap[randomChar].morse);
        } else {
            printf("\n");
        }
        strcpy(correctAnswer, morseMap[randomChar].morse); // Use correctAnswer to store the Morse code
    } else if (gameLevel == 3 || gameLevel == 4) {
        randomChar = rand() % NUM_WORDS; // Use NUM_WORDS for the range
        printf("\nEnter the word: %s", wordMorseMap[randomChar].word);
        if (gameLevel == 3) {
            printf(" or '%s' in Morse Code.\n", wordMorseMap[randomChar].morse);
        } else {
            printf("\n");
        }
        strcpy(correctAnswer, wordMorseMap[randomChar].morse); // Use correctAnswer to store the Morse code
    }

    // Reset the values
    strtInput = true; // Control for new session
    numOfCharEnt = 0; // Track number of characters entered
    playerInput[0] = '\0'; // Clear player input to receive new input
    numOfAttempts++;
    printf("Entered input: ");
}




/**
 * @brief Attempts to select a game level based on player input.
 * 
 * This function checks if the player's input matches any of the Morse code patterns
 * corresponding to level selections. If a match is found, it updates the game level,
 * indicates the selection via an LED color change, and notifies the player. If the input does
 * not match any level selection codes or if the number of characters entered does not exactly
 * match a level selection code, it prompts the player to try again.
 */
int select_level() {
    if (numOfCharEnt == 5) {
        for (int i = 1 ; i <= 4 ; i++) {
            if (!strcmp(playerInput, morseMap[i].morse)) {
                printf("\nLevel %d was selected.\n\n", i);
                gameLevel = i;
                set_LED_green(); 
                return 1;
            }
        }
        printf("Incorrect Input. \nPlease try again!!\n");
        printf("Input: ");

        // Reset the vals
        strtInput = true;
        numOfCharEnt = 0;
        playerInput[0] = '\0';
        return 0;
    }
    return 0; 
}


/**
 * @brief Retrieves the current game level.
 * 
 * This function returns the current level of the game. It's used to chcek the current progress
 * within the game.
 * 
 * @return int The current level of the game.
 */
int get_level(){
    return gameLevel;
}



/**
 * @brief Displays a summary of the player's performance.
 * 
 * This function prints out a review of the player's game performance, including the total number
 * of attempts made, the number of correcct answers provided, and the number of incorrect answers.
 */
void player_review() {
    printf("\n===================================================================\n");
    printf("Review:\n");
    printf("Number of Attempts: %d\n", numOfAttempts);
    printf("Correct answers entered: %d\n", numOfAnsCorrect);
    printf("Incorrect answers entered: %d\n", numOfAnsIncorrect);
    printf("\n===================================================================\n");
}




/**
 * @brief Marks the game as complete and displays game's outcome.
 * 
 * This function sets the game completion flag to true and then checks the number of lives left
 * to determine the outcome of the game. If there is no lives left, it declares the game
 * over. Otherwise, it delares that the player has won. It also sets the LED color to blue to
 * indicate that the game is no longer in progress and informs the player about the waiting period
 * before they can replay the game.
 */
void complete_game() { 
    if_game_completed = true;
    if (livesLeft == 0) { // Player has lost the game
        printf("\n===================================================================\n");
        printf("GAME OVER!\n");
        printf("You must wait 9 seconds in order to replay the game.\n");
        printf("===================================================================\n");
    }
    else { // Player has won the game
        set_LED_blue(); //To indicate that game isn't in progress
        printf("\n===================================================================\n");
        printf("You have won.\n\n");
        printf("You must wait 9 seconds in order to replay the game.\n");
        printf("===================================================================\n");
    }
}



/**
 * @brief Advances the game to the next level, or finishes the game if all levels are completed.
 * 
 * This function is responsible for managing the levels in the game. It increments
 * the current game level and resets relevant game state variabls such as the correct answer streak,
 * the number of characters entered, attempts made, and counts of correct and incorrect answers.
 * If the player has progressed beyond the last level, the game is marked
 * as completed by calling the complete_game function. Else, it notifies the player that they
 * have advanced to the next level.
 */
void continue_to_next_level() { 
    // Resetting necessary variables
    gameLevel++;
    correctAnsStreak = 0;
    numOfCharEnt = 0;
    numOfAttempts = 0;
    numOfAnsCorrect = 0;
    numOfAnsIncorrect = 0;

    if (gameLevel > NUMOFLEVELS) { // If all levels are finished
        complete_game();
    }
    else {
        printf("\n===================================================================\n");
        printf("You have moved to level %d!\n", gameLevel);
        printf("===================================================================\n");
    }
}

/**
 * @brief Determines the next step in the game based on the current state.
 * 
 * This function decides whether the game continues, advances to the next level, or ends
 * based on the number of lives left and the correct answer streak. If the player has no
 * lives left, the game completion routine is triggered and displays the player review.
 * If the player has a streak of 5 correct answers, it advances the game to the next level.
 * Eles, it continues on the current level by selecting a new random character or word.
 */
void proceed_in_game() {
    if (livesLeft == 0) {
        player_review();
        complete_game();
    }
    else {
        if (correctAnsStreak == 5) { // If the player has entered 5 correct answers consecutively, proceed to the next level
            player_review();
            continue_to_next_level();
        }
        else { // Normal case, continue on the current level
            choose_random_char();
        }
    }
}

/**
 * @brief Checks if the game has ended.
 * 
 * This function checks the game completion flag to determin if the game has ended.
 * It returns 1 if the game is completed, indicating that no further actions are required
 * from the player, or 0 if the game is in progress.
 */
int has_game_ended() {
    if(if_game_completed == true) { // Corrected variable name
        return 1;
    } else {
        return 0;
    }
}


/**
 * @brief Stores the Morse code input from the player.
 * 
 * This function updates the player's input based on the character code provided.
 * It appends a "-" for dash, a "." for a dot, or a " " (space) if neither a dot nor a dash
 * has been entered and input has already started. It manages the input length and
 * ensures that the Morse code sequence does not exceed the maximum length
 */
void store_input(int inputChar) { 

    if (numOfCharEnt < MAX_MORSE_SEQUENCE_LENGTH) { // Ensures the mores code sequence doesnt exceed max length 5!!!
        
        if (inputChar == 2) { // If input character is 1, it corresponds to a "-" in morse code.
            strcat(playerInput, "-"); // Append dash to the end of current player input string.
            inputlen++;
            printf("-"); // Display dash
            numOfCharEnt++; // Increment the counter for num of characters entered.

            if (strtInput == true) { // If this is the first character input, toggle start input flag (i.e input has begun).
                strtInput = false;
            }
        }
        else if (inputChar == 1) { // If the input character is 2, corresponds to a "." in morse code.
            strcat(playerInput, ".");  // Append dot to end of the current player input string.
            printf("."); // Display dot.
            inputlen++;
            numOfCharEnt++; // Increment num of characters entered.
            if (strtInput == true) {  // If this is the first character input, toggle start input flag (i.e input has begun).
                strtInput = false;
            }
        }
        // If a dot or dash has previously been entered (input has started) and current input is neither.
        // Used for space.
        else if (strtInput == false) {
            strcat(playerInput, " "); // Append a space to the end of the current player input string.
            inputlen++;
            printf(" "); // Display the space.
            numOfCharEnt++; // Increment the number of characters entered.
        }
    }
}



/**
 * @brief Checks the player's input against the correct answer.
 * 
 * This function first trims any trailing whitespace from the player's input and then
 * compares it to the correct answer. If input matches the correct answer, it
 * increments the correct answer streak and increases the player's lives.
 * Else, it decrements the player's lives and resets the correct answer streak.
 * After checking, it updates the LED color based on the remaining lives.
 */
   void check_input() {
    // Trim trailing whitespace from playerInput
    trim_trailing_whitespace(playerInput);

    // Debugging: Print inputs for verification after trimming
    printf("Debug - Trimmed Player Input: '%s', Correct Answer: '%s'\n", playerInput, correctAnswer);

    // Compare player input with the correct answer
    if(strcmp(playerInput, correctAnswer) == 0) {
        printf("Answer is correct.\n");
        numOfAnsCorrect++;
        correctAnsStreak++;
        if (livesLeft < 3) { // Ensure lives do not exceed 3
            livesLeft++;
        }
        change_LED();
    } else {
        printf("Answer is incorrect.\n");
        if (livesLeft > 0) { // Ensure lives do not go below 0
            livesLeft--;
        }
        numOfAnsIncorrect++;
        correctAnsStreak = 0;
        change_LED();
    }
    printf("Number of lives: %d.\n", livesLeft);
    printf("Number of times input answer is correct: %d.\n", correctAnsStreak);
} 



/**
 * @brief Trims trailing whitespace characters from a string.
 *
 * This function iterates through the string character by character,
 * it identifies the last non-whitespace character. It then null-terminates the string immediately
 * after this character and removes any trailing whitespace.
 *
 * @param str Pointer to the string that will be trimmed of its trailing whitespace.
 */
void trim_trailing_whitespace(char* str) {
    int index, i;
    index = -1;
    i = 0;
    while(str[i] != '\0') {
        if(str[i] != ' ' && str[i] != '\t' && str[i] != '\n') {
            index= i;
        }
        i++;
    }
    str[index + 1] = '\0';
}


/**
 * @brief Performs a quick check to validate the input against the Morse code map.
 *
 * This function compares the player's input against all entries in the Morse code map.
 * If a match is found, the func returns 1, indicating a correct match.
 * If no match is found and the input length is less than 6, it returns 2.
 * The purpose is to quickly check if the input is valid Morse code for a character.
 *
 * @return int Returns 1 if a match is found, 2 if the input length is less than 6.
 */
int quick_check(){
    bool correct = true;
    for(int i = 0; i < NUM_CHAR; i++){
        if(strcmp(playerInput, morseMap[i].morse)){
            correct = false; //set the bool to true
            break;
        }
    }
    if(correct == true){
        return 1;
    }
    if(strlen < 6){
        return 2;
    }
}


 /** @brief Gets the current time in milliseconds since the system boot.
 *
 * This function retrieves the current system time since the Raspberry Pi Pico was booted
 * and converts it into milliseconds.
 *
 * @return int The current time in milliseconds since the system boot.
 */
int get_time(){
    absolute_time_t time = get_absolute_time();
    return to_ms_since_boot(time);
}


/**
 * @brief Checks the number of lives left in the game.
 *
 * This function returns the current number of lives left for the player in th game. The number
 * of lives is decremented each time the player enters an incorrect answer and is also used to track
 * the progress and status of the game.
 *
 * @return int The current number of lives left.
 */
int check_lives_left(){
    return livesLeft;
}


/**
 * @brief Checks the current streak of correct answers.
 *
 * This function returns the number of consecutive correct answrs given by the player. It is used
 * to track the player's progress and determine when the player advances to the next level.
 *
 * @return int The current streak of correct answers.
 */
int check_streak(){
    return correctAnsStreak;
}


/**
 * @brief Main entry point of the application.
 * 
 * @return int Program exit status.
 */
int main() {
    
    stdio_init_all();              // Initialise all basic IO
   
    // welcome();
    uint32_t time_seed = time_us_32(); //takes the current time
    srand(time_seed);
    //printf("Assignment #1...\n");  // Basic print to console
    //main_asm();                    // Jump into the ASM code
    
    
    //Initialising the PIO interface
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);

    //Enter the main assembly code subroutine
    main_asm();
    return(0);
}
